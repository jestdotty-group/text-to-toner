const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')

const pkg = require('./package')
const dist = path.join(__dirname, 'dist')
module.exports = {
	target: 'web',
	mode: 'none',
	entry: path.join(__dirname, 'index.js'),
	output: {
		path: dist,
		filename: `${pkg.name}.js`
	},
	devtool: "source-map",
	resolve: {
		alias: {svelte: path.resolve('node_modules', 'svelte')},
		extensions: ['.mjs', '.js', '.svelte'],
		mainFields: ['svelte', 'browser', 'module', 'main']
	},
	module: {
		rules: [{
			test: /\.(html|svelte)$/,
			use: {loader: 'svelte-loader'}
		}]
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: pkg.name.replace(/-/g, ' ').replace(/\b\w/g, c=> c.toUpperCase()),
			favicon: 'asset/favicon.png'
		}),
	]
}
